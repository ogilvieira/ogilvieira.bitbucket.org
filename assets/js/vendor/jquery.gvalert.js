/*
 * jQuery gvalert plug-in v.0.9.1
 *
 *
 * Create by @ogilvieira
 * http://www.ogilvieira.com.br
 * 
 * License: GNU General Public License, version 3 (GPL-3.0)
 * http://www.opensource.org/licenses/gpl-3.0.html
 *
 */
"use strict"
$.gvalert = function ( text, params ) {
	var _config = {
		'type' : 'danger', // danger, success, info, warning
		'valign' : 'top', // top, bottom 
		'align' : 'center', // left, center, right
		'interval' : 8000,
		'animation': 'slide', // slide, fade, bounce
		'title' : undefined
	};
	
	if ( !text ) {
		return false;
	};

	if ( params !== undefined ) {
		$.each( params, function ( key, val ) {
			_config[key] = val;
		});
	};


	GVALERT.createAlert( text, _config );
};

var GVALERT = {
	createAlert : function ( text, config ) {
		// define key id for alert
		var id = (function () {
			return Math.floor(Math.random() * 100) + 1;
		}());

		var template = '<div class=\"gvalert-modal gv-type-'+config['type']+' gv-animation-'+config['animation']+'\" data-pos-x="'+config['valign']+'" data-pos-y="'+config['align']+'" data-gvid=\"'+id+'\">';
		if ( config['title'] ) {
			template += '<div class=\"gvalert-title\">'+config['title']+'</div>';
		};

		template += '<div class=\"gv-msg\">'+text+'</div>';
		template += '<button class=\"gvalert-btn-close\" type="button">Close</button>';
		template += '</div>';

		GVALERT.insertAlert(template, id, config['interval']);
	},
	insertAlert : function ( template, id, interval ) {
		if( $('.gvalert-modal').length !== 0 ) {
			$('.gvalert-modal').each(function( key, val ){
				var r = $(val).attr("data-gvid");
				GVALERT.removeAlert( r );
			});
		}
		
		$('body').append(template);

		$('body').delegate('.gvalert-modal[data-gvid='+id+'] .gvalert-btn-close, .gvalert-modal[data-gvid='+id+']', 'touchstart touchmove touchend touchcancel click', function(){
			GVALERT.removeAlert(id);
		});

		setTimeout(function(){
			GVALERT.removeAlert(id);
		}, interval);

	},
	removeAlert : function ( id ) {
		var target = $('.gvalert-modal[data-gvid='+id+']');
		
		target.addClass('gvalert-out');

		setTimeout(function(){
			$('.gvalert-modal[data-gvid='+id+']').remove();
		}, 300); // duration of animation

	}
};